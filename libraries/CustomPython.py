from robot.api import logger

def return_answer(arg):
    logger.info('example')
    return 42

def join_two_strings(arg1, arg2):
    return arg1 + "|" + arg2


def count_number_of_words(fname):
    num_words = 0
    with open(fname, 'r') as f:
        for line in f:
            words = line.split()
            num_words += len(words)
    logger.info("Number of words:" + str(num_words))
    return num_words